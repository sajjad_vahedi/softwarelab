echo "Test Output of the app.cpp"
g++ app.cpp -o binary

OUTPUT=`./binary`

if [ "${OUTPUT:1:1}" = "e" ]; then
    echo "Test worked successfully"
    exit 0
fi

echo "Test Output doesn't match"
exit 1

