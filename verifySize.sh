echo "Test Size of the app.cpp"
g++ app.cpp -o binary

OUTPUT=`./binary`

if [ "${#OUTPUT}" -eq 11 ]
then
    echo "Test worked successfully"
    exit 0
fi

echo "Test Size doesn't match"
exit 1

